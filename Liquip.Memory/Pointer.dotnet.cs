using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Liquip.Memory;

public partial struct Pointer
{

    public partial void Fill(byte value)
    {
        unsafe
        {
            var span = new Span<byte>((void*)Ptr, (int)Size);
            span.Fill(value);
        }
    }

}
