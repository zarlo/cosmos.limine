using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Liquip.Memory;

public struct Diff
{
    public uint Offset { get; init; }
    public Pointer Pointer { get; init; }

    public Diff(uint offset, Pointer pointer)
    {
        Offset = offset;
        this.Pointer = pointer;
    }

}

/// <summary>
/// utils for working with buffers
/// </summary>
public static unsafe class BufferUtils
{

    /// <summary>
    /// get the diffs of to blocks of memory
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static List<Diff> GetDiffs(Pointer a, Pointer b)
    {
        var minSize = Math.Min(a.Size, b.Size);

        uint offset = 0;

        var found = false;

        var diffs = new List<Diff>();

        for (uint i = 0; i < minSize; i++)
        {

            if (a[i] == b[i])
            {

                if (!found)
                {
                    offset = i;
                    found = true;
                }

            }
            else
            {
                if (found)
                {
                    found = false;
                    diffs.Add(new Diff(offset, Pointer.MakeFrom(new Address(b.GetAddress() + offset), offset - i)));
                }
            }

        }

        return diffs;
    }

    /// <summary>
    /// copies every thing from the source to the destination
    /// </summary>
    /// <param name="source"></param>
    /// <param name="destination"></param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void MemoryCopy(Pointer source, Pointer destination)
    {
        Buffer.MemoryCopy(source.Ptr, destination.Ptr, destination.Size, source.Size);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="source"></param>
    /// <param name="destination"></param>
    /// <param name="destinationIndex"></param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void MemoryCopy(
        Pointer source,
        Pointer destination,
        uint destinationIndex
    )
    {
        MemoryCopy(source, destination, destinationIndex, source.Size);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="source"></param>
    /// <param name="destination"></param>
    /// <param name="destinationIndex"></param>
    /// <param name="size"></param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void MemoryCopy(
        Pointer source,
        Pointer destination,
        uint destinationIndex,
        uint size
    )
    {
        Buffer.MemoryCopy(
            source.Ptr,
            destination.Ptr + destinationIndex,
            source.Size,
            size
        );
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="source"></param>
    /// <param name="destination"></param>
    /// <param name="destinationIndex"></param>
    /// <param name="sourceIndex"></param>
    /// <param name="size"></param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void MemoryCopy(
        Pointer source,
        Pointer destination,
        uint destinationIndex,
        uint sourceIndex,
        uint size
    )
    {
        Buffer.MemoryCopy(
            source.Ptr + sourceIndex,
            destination.Ptr + destinationIndex,
            source.Size,
            size
        );
    }
}
