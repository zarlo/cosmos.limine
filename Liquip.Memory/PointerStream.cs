using System;
using System.IO;

namespace Liquip.Memory;

/// <summary>
/// a pointer as a stream
/// </summary>
public class PointerStream : Stream
{
    public PointerStream(Pointer ptr)
    {
        Pointer = ptr;
    }

    public Pointer Pointer { get; init; }
    public unsafe uint* Ptr => Pointer.Ptr;

    /// <inheritdoc />
    public override bool CanRead { get; } = true;

    /// <inheritdoc />
    public override bool CanSeek { get; } = true;

    /// <inheritdoc />
    public override bool CanWrite { get; } = true;

    /// <inheritdoc />
    public override long Length => Pointer.Size;

    /// <inheritdoc />
    public override long Position { get; set; }

    public override void Flush()
    {
    }

    /// <inheritdoc />
    public override long Seek(long offset, SeekOrigin origin)
    {
        switch (origin)
        {
            case SeekOrigin.Begin:
                Position = offset;
                break;
            case SeekOrigin.Current:
                Position += offset;
                break;
            case SeekOrigin.End:
                Position = Length - offset;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(origin), origin, null);
        }

        if (Position < 0 || Position > Length)
        {
            throw new IndexOutOfRangeException();
        }

        return Position;
    }

    public override void SetLength(long value)
    {
    }

    /// <inheritdoc />
    public override int Read(byte[] buffer, int offset, int count)
    {
        Pointer.CopyTo(buffer, (uint)offset, (uint)Position, (uint)count);
        return count;
    }

    /// <inheritdoc />
    public override void Write(byte[] buffer, int offset, int count)
    {
        BufferUtils.MemoryCopy(Pointer.MakeFrom(buffer), Pointer, (uint)Position, (uint)(count - offset));
    }
}
