using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cosmos.Core;

namespace Liquip.Memory;

public partial struct Pointer
{

    public partial void Fill(byte value)
    {
        unsafe
        {
            MemoryOperations.Fill(Ptr, value, (int)Size);
        }
    }

}
