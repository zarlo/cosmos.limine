using System;

namespace Liquip.Memory;

/// <summary>
///
/// </summary>
public sealed class CString: IDisposable
{

    /// <summary>
    /// the raw pointer
    /// </summary>
    public Pointer Pointer;


    /// <summary>
    /// if this cstring will be cleaned up by the GC
    /// </summary>
    public bool AutoCleanUp { get; private set; }


    /// <summary>
    ///
    /// </summary>
    /// <param name="ptr">pointer to the string</param>
    /// <param name="length">length of string in bytes</param>
    public CString(Address ptr, uint length)
    {
        Pointer = Pointer.MakeFrom(ptr, length);
        AutoCleanUp = false;
    }

    public void Dispose()
    {
        if (AutoCleanUp)
        {
            Pointer.Free();
        }
    }
}
