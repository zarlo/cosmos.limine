using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
#if COSMOS
using Cosmos.Core;
#endif
namespace Liquip.Memory;

/// <summary>
/// a manages pointer
/// </summary>
[StructLayout(LayoutKind.Auto)]
public partial struct Pointer
{
    /// <summary>
    ///     defaults as <see cref="Null" />
    /// </summary>
    public static readonly Pointer Default = Null;

    /// <summary>
    ///     the Null pointer
    /// </summary>
    public static unsafe Pointer Null => new(null, 0);

    /// <summary>
    ///     get a pointer to of an object with the given size
    /// </summary>
    /// <param name="size">size in bytes</param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe Pointer New(uint size)
    {
        return new Pointer(NativeMemory.Alloc(size), size);
    }


    /// <summary>
    ///     get a pointer to of an object with the given size
    /// </summary>
    /// <param name="size">size in bytes</param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe Pointer New(int size) => New((uint)size);

    /// <summary>
    ///     get a pointer to of an object with the given size
    /// </summary>
    /// <param name="ptr"></param>
    /// <param name="size">size in bytes</param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe Pointer MakeFrom(uint* ptr, uint size)
    {
        var p = new Pointer(ptr, size);
        return p;
    }

    /// <summary>
    ///     get a pointer to of an object with the given size
    /// </summary>
    /// <param name="ptr"></param>
    /// <param name="size">size in bytes</param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static unsafe Pointer MakeFrom(Address ptr, uint size)
    {
        var p = new Pointer((uint*)(uint)ptr, size);
        return p;
    }

    /// <summary>
    ///     get a pointer to of a byte array
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Pointer MakeFrom(byte[] data)
    {
        var p = new Pointer(data);
        return p;
    }

    private unsafe Pointer(byte[] buffer)
    {
        fixed (byte* ptr = buffer)
        {
            Ptr = (uint*)ptr;
        }
#if COSMOS
        GCImplementation.IncRootCount((ushort*)Ptr);
#endif
        Size = (uint)buffer.Length;
    }

    private unsafe Pointer(void* ptr, uint size) : this((uint*)ptr, size)
    {
    }

    private unsafe Pointer(uint* ptr, uint size)
    {
        Ptr = ptr;
#if COSMOS

        GCImplementation.IncRootCount((ushort*)Ptr);
#endif
        Size = size;
    }

    /// <summary>
    /// the raw pointer
    /// </summary>
    public unsafe uint* Ptr { get; private set; }

    /// <summary>
    /// the size of the object
    /// </summary>
    public uint Size { get; }

    /// <summary>
    ///     Resize the given pointer
    /// </summary>
    /// <param name="size"></param>
    /// <exception cref="Exception"></exception>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Resize(uint size)
    {
        if (Equals(this, Default))
        {
            throw new Exception("you cant resize null");
        }

        unsafe
        {

            Ptr = (uint*)NativeMemory.Realloc(Ptr, size);
#if COSMOS
            GCImplementation.IncRootCount((ushort*)Ptr);
#endif
        }
    }


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Resize(ulong size) => Resize((uint)size);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Resize(long size) => Resize((ulong)size);
    /// <summary>
    /// casts to a int
    /// </summary>
    /// <returns></returns>
    public int ToInt()
    {
        unsafe
        {
            return (int)Ptr;
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public unsafe void CopyTo(
        byte[] buffer,
        uint destinationIndex,
        uint sourceIndex,
        uint destinationSize
    )
    {
        var temp = new byte[destinationSize];
        Marshal.Copy(new IntPtr(Ptr), temp, (int)sourceIndex, (int)destinationSize);
        Array.Copy(temp, 0, buffer, (int)destinationIndex, destinationSize);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public unsafe void CopyTo(
        uint* destination,
        uint destinationIndex,
        uint sourceIndex,
        uint destinationSize
    )
    {
        Buffer.MemoryCopy(Ptr + sourceIndex, destination + destinationIndex, destinationSize, Size);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void CopyTo(
        Pointer destination,
        uint destinationIndex,
        uint sourceIndex,
        uint size
    )
    {
        BufferUtils.MemoryCopy(this, destination, destinationIndex, sourceIndex, size);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public partial void Fill(byte value);

    public Address GetAddress()
    {
        unsafe
        {
            return (Address)(uint)Ptr;
        }
    }

    public ref uint this[ulong index]
    {
        get
        {
            unsafe
            {
                return ref Ptr[index];
            }
        }
    }

    public ref uint this[uint index]
    {
        get
        {
            unsafe
            {
                return ref Ptr[index];
            }
        }
    }

    public static unsafe explicit operator byte*(Pointer ptr)
    {
        return (byte*)ptr.Ptr;
    }

    public static unsafe explicit operator uint*(Pointer ptr)
    {
        return ptr.Ptr;
    }

    public static unsafe explicit operator ushort*(Pointer ptr)
    {
        return (ushort*)ptr.Ptr;
    }

    public static unsafe explicit operator ulong*(Pointer ptr)
    {
        return (ulong*)ptr.Ptr;
    }

    public void Free()
    {
        unsafe
        {
            NativeMemory.Free(Ptr);
        }
    }


    public override bool Equals(object? obj)
    {
        unsafe
        {
            switch (obj)
            {
                case null when Ptr == Default.Ptr:
                    return true;
                case null:
                    return false;
            }

            if (obj.GetType() != typeof(Pointer))
            {
                return false;
            }

            var o = (Pointer)obj;
            return o.Ptr == Ptr && o.Size == Size;
        }
    }

    public override int GetHashCode()
    {
        unsafe
        {
            return (int)Ptr;
        }
    }

    public Span<byte> ToSpan()
    {
        unsafe
        {
            return new Span<byte>(Ptr, (int)Size);
        }
    }


}
