using System.Data.SqlTypes;
using Cosmos.Coroutines.System.Coroutines;

namespace Liquip.Cosmos.Coroutines;

/// <summary>
/// Yield to let another Coroutine to run
/// </summary>
public class Yield : CoroutineControlPoint
{
    public override bool CanContinue { get; } = true;
}
