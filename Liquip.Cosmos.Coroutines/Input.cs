﻿using System.Text;
using Cosmos.Coroutines.System.Coroutines;
using Cosmos.System;


namespace Liquip.Cosmos.Coroutines;

/// <summary>
/// get input
/// </summary>
public class Input
{

    /// <summary>
    ///
    /// </summary>
    /// <returns></returns>
    public static IEnumerator<CoroutineControlPoint?> ReadKey(Action<KeyEvent> callback)
    {
        while (true)
        {

            yield return new Yield();
            if (KeyboardManager.TryReadKey(out var data))
            {
                callback(data);
                yield break;
            }

        }
    }

    /// <summary>
    /// read a whole line
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    public static IEnumerator<CoroutineControlPoint?> ReadLine(Action<string> callback)
    {
        var line = new StringBuilder();
        while (true)
        {

            yield return new Yield();
            if (KeyboardManager.TryReadKey(out var data))
            {
                if (data.Key == ConsoleKeyEx.Enter)
                {
                    callback(line.ToString());
                    yield break;
                }
                else
                {
                    line.Append(data.KeyChar);
                }
            }

        }
    }

}
