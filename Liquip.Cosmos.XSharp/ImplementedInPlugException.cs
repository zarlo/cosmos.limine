using System;

namespace Liquip.Cosmos.XSharp;

public class ImplementedInPlugException : NotImplementedException
{
    public ImplementedInPlugException()
    {
    }

    public ImplementedInPlugException(Type type) : base(type.FullName)
    {
    }
}
