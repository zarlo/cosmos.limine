using System.Collections.Generic;
using System.Linq;
using XSharp;
using XSharp.Assembler;

namespace Liquip.Cosmos.XSharp.Fluent;

public static class LiteralAssemblerCodeEx
{
    public static FluentXSharpX86 LiteralCode(this FluentXSharpX86 me, string line, params object?[] args)
    {
        var data = new List<object?>();
        foreach (var arg in args)
        {
            XS.Comment(arg?.ToString() ?? "null");
        }
        _ = new LiteralAssemblerCode(string.Format(line, args.ToArray()));
        return me;
    }

    public static FluentXSharpX86 LiteralCode(this FluentXSharpX86 me, string line)
    {
        _ = new LiteralAssemblerCode(line);
        return me;
    }

    public static FluentXSharpX86 LiteralCode(this FluentXSharpX86 me, IEnumerable<string> lines)
    {
        foreach (var line in lines)
        {
            _ = new LiteralAssemblerCode(line);
        }

        return me;
    }
}
