using System;
using XSharp;

namespace Liquip.Cosmos.XSharp;

public class PlugArgument
{
    public static readonly XSRegisters.Register32 Register = XSRegisters.EBP;

    public PlugArgument(string name, int offset, Type? type)
    {
        if (string.IsNullOrWhiteSpace(name))
        {
            throw new Exception();
        }

        Type = type;
        Offset = offset;
        Name = name;
    }

    /// <summary>
    /// type used
    /// </summary>
    public Type? Type { get; init; }
    /// <summary>
    /// offset of item
    /// </summary>
    public int Offset { get; init; }
    /// <summary>
    /// a name
    /// </summary>
    public string Name { get; init; }
}
