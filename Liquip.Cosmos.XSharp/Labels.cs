using Liquip.Cosmos.XSharp.Fluent;

namespace Liquip.Cosmos.XSharp;

public static class Labels
{
    public static Label DebugStub_SendCommandOnChannel = new Label(nameof(DebugStub_SendCommandOnChannel));
}
