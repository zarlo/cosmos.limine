using System.Runtime.Intrinsics.X86;
using IL2CPU.API;
using IL2CPU.API.Attribs;
using Liquip.Cosmos.XSharp;
using Liquip.Cosmos.XSharp.Fluent;
using static XSharp.XSRegisters;

namespace Liquip.Cosmos.Plugs.System.Runtime.Intrinsics.System.Runtime.Intrinsics.X86;

/// <summary>
/// X86BasePlug
/// </summary>
[Plug(target: typeof(X86Base))]
public class X86BasePlug
{

    /// <summary>
    /// raw cpuid call
    /// </summary>
    /// <param name="cpuInfo"></param>
    /// <param name="functionId"></param>
    /// <param name="subFunctionId"></param>
    public static unsafe void __cpuidex(int* cpuInfo, int functionId, int subFunctionId)
    {
        int Eax = 0;
        int Ebx = 0;
        int Ecx = 0;
        int Edx = 0;

        Raw(functionId, subFunctionId, ref Eax, ref Ebx, ref Ecx, ref Edx);

        cpuInfo[0] = Eax;
        cpuInfo[1] = Ebx;
        cpuInfo[2] = Ecx;
        cpuInfo[3] = Edx;
    }

    [Inline]
    public static void Raw(int type, int subType, ref int eax, ref int ebx, ref int ecx, ref int edx)
    {
        var args = ArgumentBuilder.Inline();
        FluentXSharp.NewX86()
            .SetPointer(EAX, args.GetArg(nameof(type)))
            .SetPointer(ECX, args.GetArg(nameof(subType)))
            .Cpuid()
            .SetPointer(EDI, args.GetArg(nameof(eax)))
            .Set(EDI, EAX, true)
            .SetPointer(EDI, args.GetArg(nameof(ebx)))
            .Set(EDI, EBX, true)
            .SetPointer(EDI, args.GetArg(nameof(ecx)))
            .Set(EDI, ECX, true)
            .SetPointer(EDI, args.GetArg(nameof(edx)))
            .Set(EDI, EDX, true);
    }

}
