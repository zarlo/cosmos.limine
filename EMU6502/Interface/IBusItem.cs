namespace EMU6502.Interface;

public interface IBusItem
{

    public byte ReadByte(Cpu cpu, AddressSpace addressSpace, ushort address);

    public ushort ReadWord(Cpu cpu, AddressSpace addressSpace, ushort address);

    public void WriteByte(Cpu cpu, AddressSpace addressSpace, ushort address, byte value);

    public void WriteWord(Cpu cpu, AddressSpace addressSpace, ushort address, ushort value);

}
