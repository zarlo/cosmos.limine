namespace EMU6502.Interface;

public interface IOpCode
{
    public OpCode OpCode { get; }
    public void Execute(Cpu cpu, AddressSpace addressSpace);
}
