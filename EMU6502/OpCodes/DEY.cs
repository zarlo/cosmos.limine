using EMU6502.Interface;

namespace EMU6502.OpCodes;

public class DEY: IOpCode
{
    public OpCode OpCode { get; } = OpCode.DEY;
    public void Execute(Cpu cpu, AddressSpace addressSpace)
    {
        cpu.Y--;
    }
}
