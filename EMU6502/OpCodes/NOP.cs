using EMU6502.Interface;

namespace EMU6502.OpCodes;

public class NOP : IOpCode
{
    public OpCode OpCode { get; } = OpCode.NOP;

    public void Execute(Cpu cpu, AddressSpace addressSpace)
    {
        cpu.PC++;
        // NOP do nothing
    }
}
