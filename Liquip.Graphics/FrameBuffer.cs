using System.Runtime.InteropServices;
using Liquip.Memory;

namespace Liquip.Graphics;
/// <summary>
/// stores info about a framebuffer
/// </summary>
[StructLayout(LayoutKind.Auto)]
public struct FrameBuffer
{
    /// <summary>
    /// pointer to the frame buffer
    /// </summary>
    public Pointer Ptr;
    /// <summary>
    /// the width of the screen
    /// </summary>
    public uint Width;
    /// <summary>
    ///
    /// </summary>
    public uint Height;
    /// <summary>
    ///
    /// </summary>
    public uint? PixelSize;

    /// <summary>
    ///
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public uint Offset(uint x, uint y) => ((y * Width) + x) * (PixelSize ?? 1);

}
