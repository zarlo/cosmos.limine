using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Liquip.Memory;

namespace Liquip.Common;

/// <summary>
/// Little Endian Base 128 is a variable-length code compression used to store arbitrarily large integers in a small number of bytes.
/// </summary>
[StructLayout(LayoutKind.Sequential, Pack = 1)]
public readonly struct Leb128 : IComparable<Leb128>, IConvertible, IEquatable<Leb128>
{
    /// <summary>
    /// denotes if its signed
    /// </summary>
    public readonly bool IsSigned;

    /// <summary>
    /// the raw data
    /// </summary>
    public readonly byte[] Raw;

    /// <summary>
    ///
    /// </summary>
    /// <param name="data"></param>
    /// <param name="isSigned"></param>
    public Leb128(Span<byte> data, bool isSigned = false)
    {
        IsSigned = isSigned;
        var index = 0;
        byte b;
        do
        {
            b = data[index++];
        } while ((b & 0x80) != 0);
        Raw = new byte[index];
        for (int i = 0; i < index; i++)
        {
            Raw[i] = data[i];
        }
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="ptr">memory pointer</param>
    /// <param name="offset">the offset of the pointer to start at</param>
    /// <param name="isSigned">if its signed or not</param>
    public Leb128(ref Pointer ptr, int offset = 0, bool isSigned = false): this(ref ptr, out _, offset, isSigned)
    {

    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="ptr">memory pointer</param>
    /// <param name="size">the size in byte of the number</param>
    /// <param name="offset">the offset of the pointer to start at</param>
    /// <param name="isSigned">if its signed or not</param>
    public Leb128(ref Pointer ptr, out int size, int offset = 0, bool isSigned = false)
    {
        IsSigned = isSigned;
        var index = offset;
        byte b;
        do
        {
            unsafe
            {
                b = (byte)(byte*)ptr.Ptr[index++];
            }
        } while ((b & 0x80) != 0);
        Raw = new byte[index];
        for (var i = 0; i < index; i++)
        {
            unsafe
            {
                Raw[i] = (byte)(byte*)ptr.Ptr[i];
            }
        }

        size = Raw.Length;
    }


    /// <summary>
    /// gets a ulong
    /// </summary>
    /// <returns></returns>
    private ulong GetLong()
    {
        ulong result = 0;
        byte shift = 0;
        byte b;
        var index = 0;
        do
        {
            b = Raw[index++];
            result |= ((ulong)0x7F & b) << shift;
            shift += 7;

        } while ((b & 0x80) != 0);

        return result;
    }

    /// <summary>
    /// get a long
    /// </summary>
    /// <returns></returns>
    private long GetSignedLong()
    {
        ulong result = 0;
        byte shift = 0;
        byte b;
        var index = 0;
        do
        {
            b = Raw[index++];
            result |= ((ulong)0x7F & b) << shift;
            shift += 7;

        } while ((b & 0x80) != 0);

        /* sign bit of byte is second high order bit (0x40) */
        if ((b & 0x40) != 0)
        {
            result |= ~(ulong)0 << shift;
        }

        return (long)result;
    }

    /// <summary>
    /// make a Leb128 from a ulong
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Leb128 From(ulong value) {
        var data = new List<byte>();
        var more = true;

        while (more) {
            var chunk = (byte)(value & 0x7fUL); // extract a 7-bit chunk
            value >>= 7;

            more = value != 0;
            if (more) { chunk |= 0x80; } // set msb marker that more bytes are coming

            data.Add(chunk);
        };

        return new Leb128(data.ToArray(), false);
    }

    /// <summary>
    /// make a Leb128 from a long
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Leb128 From(long value)
    {
        var data = new List<byte>();
        var more = true;

        while (more) {
            var chunk = (byte)(value & 0x7fL); // extract a 7-bit chunk
            value >>= 7;

            var signBitSet = (chunk & 0x40) != 0; // sign bit is the msb of a 7-bit byte, so 0x40
            more = !((value == 0 && !signBitSet) || (value == -1 && signBitSet));
            if (more) { chunk |= 0x80; } // set msb marker that more bytes are coming

            data.Add(chunk);
        };

        return new Leb128(data.ToArray(), true);
    }


    /// <inheritdoc />
    public int CompareTo(Leb128 other)
    {
        if (IsSigned)
        {
            var signed = GetSignedLong();
            var signedOther = other.GetSignedLong();
            if (signed == signedOther) return 0;
            if (signed > signedOther) return 1;
            if (signed < signedOther) return -1;
        }

        var unsigned = GetLong();
        var unsignedOther = other.GetLong();
        if (unsigned == unsignedOther) return 0;
        if (unsigned > unsignedOther) return 1;
        if (unsigned < unsignedOther) return -1;

        throw new NotSupportedException();

    }

    /// <inheritdoc />
    public TypeCode GetTypeCode()
    {
        return IsSigned ? TypeCode.Int64 : TypeCode.UInt64;
    }

    /// <inheritdoc />
    public char ToChar(IFormatProvider? provider)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public DateTime ToDateTime(IFormatProvider? provider)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public decimal ToDecimal(IFormatProvider? provider)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public double ToDouble(IFormatProvider? provider)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public float ToSingle(IFormatProvider? provider)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public bool ToBoolean(IFormatProvider? provider)
    {
        return 1 == ToInt64(provider);
    }

    /// <inheritdoc />
    public string ToString(IFormatProvider? provider)
    {
        return IsSigned ? GetSignedLong().ToString(provider) : GetLong().ToString(provider);
    }

    /// <inheritdoc />
    public sbyte ToSByte(IFormatProvider? provider)
    {
        return (IsSigned ? (sbyte)GetSignedLong() : (sbyte)GetLong());
    }

    /// <inheritdoc />
    public short ToInt16(IFormatProvider? provider)
    {
        return (IsSigned ? (short)GetSignedLong() : (short)GetLong());
    }

    /// <inheritdoc />
    public int ToInt32(IFormatProvider? provider)
    {
        return (IsSigned ? (int)GetSignedLong() : (int)GetLong());
    }

    /// <inheritdoc />
    public long ToInt64(IFormatProvider? provider)
    {
        return (IsSigned ? GetSignedLong() : (long)GetLong());
    }

    /// <inheritdoc />
    public object ToType(Type conversionType, IFormatProvider? provider)
    {

        if (conversionType == typeof(byte))
        {
            return ToByte(provider);
        }

        if (conversionType == typeof(ushort))
        {
            return ToUInt16(provider);
        }

        if (conversionType == typeof(uint))
        {
            return ToUInt32(provider);
        }

        if (conversionType == typeof(ulong))
        {
            return ToUInt64(provider);
        }

        //

        if (conversionType == typeof(sbyte))
        {
            return ToSByte(provider);
        }

        if (conversionType == typeof(short))
        {
            return ToInt16(provider);
        }

        if (conversionType == typeof(int))
        {
            return ToInt32(provider);
        }

        if (conversionType == typeof(long))
        {
            return ToInt64(provider);
        }

        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public byte ToByte(IFormatProvider? provider)
    {
        return (IsSigned ? (byte)GetSignedLong() : (byte)GetLong());
    }

    /// <inheritdoc />
    public ushort ToUInt16(IFormatProvider? provider)
    {
        return (IsSigned ? (ushort)GetSignedLong() : (ushort)GetLong());
    }

    /// <inheritdoc />
    public uint ToUInt32(IFormatProvider? provider)
    {
        return (IsSigned ? (uint)GetSignedLong() : (uint)GetLong());
    }

    /// <inheritdoc />
    public ulong ToUInt64(IFormatProvider? provider)
    {
        return (IsSigned ? (ulong)GetSignedLong() : GetLong());
    }

    /// <inheritdoc />
    public bool Equals(Leb128 other)
    {
        return this.IsSigned == other.IsSigned && this.Raw == other.Raw;
    }
}
