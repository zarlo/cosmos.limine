namespace Liquip.WASM;

/// <summary>
/// a stack
/// </summary>
/// <typeparam name="T"></typeparam>
public struct Stack<T>
{
    /// <summary>
    /// the stack
    /// </summary>
    public T[] ValueStack { get; set; }
    /// <summary>
    /// the current pointer
    /// </summary>
    public uint StackPtr { get; set; }

    /// <summary>
    /// push to the stack
    /// </summary>
    /// <param name="value"></param>
    public void Push(T value) => ValueStack[StackPtr++] = value;

    /// <summary>
    /// get the item off the stack
    /// </summary>
    /// <returns></returns>
    public T Pop() => ValueStack[StackPtr--];

}
