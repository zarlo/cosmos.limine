﻿using System;
using Liquip.Memory;

namespace Liquip.WASM;

/// <summary>
/// Wasm memory block
/// </summary>
public class WasmMemory
{
    /// <summary>
    /// max amount of pages
    /// </summary>
    public const int PageCap = 1024;
    /// <summary>
    /// default page size
    /// </summary>
    public const int PageSize = 65536;
    /// <summary>
    /// pointer to the memory
    /// </summary>
    public Pointer Ptr;

    /// <summary>
    /// starting amount of pages
    /// </summary>
    public int MinPages;
    /// <summary>
    /// upper limit
    /// </summary>
    public int MaxPages;
    /// <summary>
    /// current page count
    /// </summary>
    public int CurrentPages;

    /// <summary>
    ///
    /// </summary>
    /// <param name="minPages"></param>
    /// <param name="maxPages"></param>
    /// <exception cref="Exception"></exception>
    public WasmMemory(int minPages, int maxPages)
    {
        if (minPages > PageCap)
        {
            throw new Exception("Out of memory!");
        }

        MinPages = minPages;
        MaxPages = maxPages;
        CurrentPages = MinPages;

        Ptr = Pointer.New(PageSize * CurrentPages);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="m"></param>
    /// <returns></returns>
    public bool CompatibleWith(WasmMemory m)
    {
        return MinPages == m.MinPages && MaxPages == m.MaxPages;
    }

    /// <summary>
    ///
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return "<memory min: " + MinPages + ", max: " + MaxPages + ", cur: " + CurrentPages + ">";
    }

    /// <summary>
    /// set a single byte
    /// </summary>
    /// <param name="offset"></param>
    /// <param name="b"></param>
    /// <exception cref="TrapException"></exception>
    public void Set(ulong offset, byte b)
    {
        if (offset < Ptr.Size)
        {
            Ptr[(uint)offset] = b;
        }
        else
        {
            throw new TrapException("out of bounds memory access");
        }
    }

    /// <summary>
    /// copies data into a buffer
    /// </summary>
    /// <param name="offset"></param>
    /// <param name="bytes"></param>
    /// <param name="data">buffer</param>
    /// <exception cref="TrapException"></exception>
    public void GetBytes(ulong offset, ulong bytes, byte[] data)
    {
        if (offset + bytes <= Ptr.Size)
        {
            Ptr.CopyTo(data, 0, 0, (uint)bytes);
        }

        throw new TrapException("out of bounds memory access");
    }

    /// <summary>
    /// copies data
    /// </summary>
    /// <param name="offset"></param>
    /// <param name="bytes"></param>
    /// <returns></returns>
    /// <exception cref="TrapException"></exception>
    public byte[] GetBytes(ulong offset, ulong bytes)
    {
        if (offset + bytes <= Ptr.Size)
        {
            var buffer = new byte[bytes];
            Ptr.CopyTo(buffer, 0, 0, (uint)bytes);
            return buffer;
        }

        throw new TrapException("out of bounds memory access");
    }

    /// <summary>
    /// copies data into the buffer
    /// </summary>
    /// <param name="offset"></param>
    /// <param name="bytes"></param>
    /// <exception cref="TrapException"></exception>
    public void SetBytes(ulong offset, byte[] bytes)
    {
        if (offset + (ulong)bytes.Length <= Ptr.Size)
        {
            Pointer.MakeFrom(bytes).CopyTo(Ptr, (uint)offset, (uint)0, (uint)bytes.Length);
        }
        else
        {
            throw new TrapException("out of bounds memory access");
        }
    }

    /// <summary>
    /// adds pages
    /// </summary>
    /// <param name="pages"></param>
    /// <returns></returns>
    public uint Grow(uint pages)
    {
        if ((MaxPages != 0 && CurrentPages + pages > MaxPages) || CurrentPages + pages > PageCap)
        {
            return 0xFFFFFFFF;
        }

        if (CurrentPages + pages < CurrentPages)
        {
            return 0xFFFFFFFF;
        }

        if (CurrentPages + pages == CurrentPages)
        {
            return (uint)CurrentPages;
        }

        Ptr.Resize((CurrentPages + pages) * 65536);

        CurrentPages += (int)pages;
        return (uint)(CurrentPages);
    }
}
