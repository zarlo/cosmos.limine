using Liquip.WASM.CPU;

namespace Liquip.WASM.Interfaces;

/// <summary>
///
/// </summary>
public interface IInstruction
{
    /// <summary>
    /// run
    /// </summary>
    /// <param name="state"></param>
    public void Run(ref CpuState state);
}
