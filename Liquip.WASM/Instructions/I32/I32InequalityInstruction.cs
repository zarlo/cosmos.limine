using Liquip.WASM.CPU;
using Liquip.WASM.Interfaces;

namespace Liquip.WASM.Instructions.I32;

[OpCode(0x47)]
public class I32InequalityInstruction : IInstruction
{
    public void Run(ref CpuState state)
    {
        if (state.ValueStack.Pop().i32 != state.ValueStack.Pop().i32)
        {
            state.ValueStack.Push(WasmValue.From(1));
        }
        else
        {
            state.ValueStack.Push(WasmValue.From(0));
        }
    }
}
