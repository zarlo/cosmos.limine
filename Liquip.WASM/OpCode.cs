using System;

namespace Liquip.WASM;

public class OpCodeAttribute : Attribute
{

    public OpCodeAttribute(ulong opcode)
    {
        OpCode = opcode;
    }

    public ulong OpCode { get; set; }
}
