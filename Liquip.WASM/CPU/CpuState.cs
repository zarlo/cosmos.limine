namespace Liquip.WASM.CPU;

/// <summary>
///
/// </summary>
public struct CpuState
{
    /// <summary>
    /// value stack
    /// </summary>
    public Stack<WasmValue> ValueStack;

    /// <summary>
    /// call stack
    /// </summary>
    public Stack<uint> CallStack;

    /// <summary>
    /// the current InstructionPointer
    /// </summary>
    public uint InstructionPointer;

    /// <summary>
    /// Cache for use in the CPU
    /// </summary>
    public WasmValue CacheA;
    /// <summary>
    /// Cache for use in the CPU
    /// </summary>
    public WasmValue CacheB;
    /// <summary>
    /// Cache for use in the CPU
    /// </summary>
    public WasmValue CacheC;
    /// <summary>
    /// Cache for use in the CPU
    /// </summary>
    public WasmValue CacheD;

}


